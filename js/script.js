$(function(){
	 $("#accordion").accordion({
        collapsible: true,
        heightStyle: "content"
    });
    
    $(".testimonials").owlCarousel({
        items: 2,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
    });
    
    $(".insta-posts").owlCarousel({
        items:1,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
    });
    $(".contents").owlCarousel({
        items:1,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
    });
    
    $(".our-clients").owlCarousel({
        items: 6,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        responsive: {
            0:{
                items: 2,
                slideBy: 1,
            },
            480:{
                items: 4,
                slideBy: 2,
            },
            768:{
                items: 6,
                slideBy: 3,
            }
        }
    });
    $(".image1").hover(function(){
		
    		$("#block1").css("display", "block");
		}, function(){
    		$("#block1").css("display", "none");
	});
	$(".image2").hover(function(){
		
    		$("#block2").css("display", "block");
		}, function(){
    		$("#block2").css("display", "none");
	});
	$(".image3").hover(function(){
		
    		$("#block3").css("display", "block");
		}, function(){
    		$("#block3").css("display", "none");
	});
});